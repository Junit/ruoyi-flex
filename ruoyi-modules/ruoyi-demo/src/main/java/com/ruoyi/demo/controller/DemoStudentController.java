package com.ruoyi.demo.controller;

import java.util.List;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.common.orm.core.page.TableDataInfo;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.web.core.BaseController;
import com.ruoyi.common.core.core.domain.AjaxResult;
import com.ruoyi.demo.domain.DemoStudent;
import com.ruoyi.demo.service.IDemoStudentService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

/**
 * 学生信息单表(mb)Controller
 *
 * @author 数据小王子
 * 2023-07-11
 */
@RestController
@RequestMapping("/demo/student")
public class DemoStudentController extends BaseController
{
    @Resource
    private IDemoStudentService demoStudentService;

    /**
     * 查询学生信息单表(mb)列表
     */
    @SaCheckPermission("demo:student:list")
    @GetMapping("/list")
    public TableDataInfo list(DemoStudent demoStudent)
    {
        startPage();
        List<DemoStudent> list = demoStudentService.selectDemoStudentList(demoStudent);
        return getDataTable(list);
    }

    /**
     * 导出学生信息单表(mb)列表
     */
    @SaCheckPermission("demo:student:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, DemoStudent demoStudent)
    {
        List<DemoStudent> list = demoStudentService.selectDemoStudentList(demoStudent);
        ExcelUtil<DemoStudent> util = new ExcelUtil<>(DemoStudent.class);
        util.exportExcel(response, list, "学生信息单表(mb)数据");
    }

    /**
     * 获取学生信息单表(mb)详细信息
     */
    @SaCheckPermission("demo:student:query")
    @GetMapping(value = "/{studentId}")
    public AjaxResult getInfo(@PathVariable("studentId") Long studentId)
    {
        return success(demoStudentService.selectDemoStudentByStudentId(studentId));
    }

    /**
     * 新增学生信息单表(mb)
     */
    @SaCheckPermission("demo:student:add")
    @PostMapping
    public AjaxResult add(@RequestBody DemoStudent demoStudent)
    {
        return toAjax(demoStudentService.insertDemoStudent(demoStudent));
    }

    /**
     * 修改学生信息单表(mb)
     */
    @SaCheckPermission("demo:student:edit")
    @PutMapping
    public AjaxResult edit(@RequestBody DemoStudent demoStudent)
    {
        return toAjax(demoStudentService.updateDemoStudent(demoStudent));
    }

    /**
     * 删除学生信息单表(mb)
     */
    @SaCheckPermission("demo:student:remove")
    @DeleteMapping("/{studentIds}")
    public AjaxResult remove(@PathVariable Long[] studentIds)
    {
        return toAjax(demoStudentService.deleteDemoStudentByStudentIds(studentIds));
    }
}
